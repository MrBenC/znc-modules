/*
 *	Description:
 *
 *		This module is an attempt to prevent forced channel parting.
 *	This includes, but is not limited to commands such as /remove,
 *	/fpart, and /sapart .
 *
 *		This module will not defeat bans that were placed immediately
 *	prior to the part or rejoin, though you'll at least receive the
 *	error about being banned from the channel.
 *
 */
 
#include <znc/Client.h>
#include <znc/Chan.h>
#include <znc/Modules.h>
#include <znc/IRCNetwork.h>

class CAntiForcePartMod : public CModule {
public:

	MODCONSTRUCTOR (CAntiForcePartMod) {}
	
	virtual ~CAntiForcePartMod() {}
	
	CString partMsg;
	
	bool OnLoad(const CString& sArgsi, CString& sMessage) {
		partMsg = partMsg.RandomString(255);
		
		return true;
	}
	
	EModRet OnUserRaw(CString& sLine) {
		if (sLine.Token(0) == "PART") {
			partMsg = sLine.Token(2, true).TrimLeft_n(":");
			
//			PutModule(sLine.Token(2, true));
		}
		
		return CONTINUE;
	}
	
	void OnPart(const CNick& Nick, CChan& Channel, const CString& sMessage) {
		if (!(Nick.GetNick() == m_pNetwork->GetIRCNick().GetNick() && sMessage.AsLower().WildCmp("*" + partMsg.AsLower() + "*"))) {
//			PutModule("Matches: No");
//			PutModule("partMsg = " + partMsg + " | sMessage = " + sMessage);
//			PutModule("m_pNetwork->GetIRCNick().GetNick() = " + m_pNetwork->GetIRCNick().GetNick() + " | Nick.GetNick() = " + Nick.GetNick());
			PutIRC("JOIN " + Channel.GetName() + " " + Channel.GetKey());
		}
//		else {
//			PutModule("Matches: Yes");
//			PutModule("partMsg = " + partMsg + " | sMessage = " + sMessage);
//			PutModule("m_pNetwork->GetIRCNick().GetNick() = " + m_pNetwork->GetIRCNick().GetNick() + " | Nick.GetNick() = " + Nick.GetNick());
			//PutIRC("JOIN " + Channel.GetName() + " " + Channel.GetKey());
//		}
		
		partMsg = partMsg.RandomString(255);
	}
	
/*	void OnModCommand(const CString& sCommand) {
		if (sCommand.Token(0) != "") {
			PutModule(partMsg);
		}
	} */
	
	
};

template<> void TModInfo<CAntiForcePartMod>(CModInfo& Info) {
	Info.SetWikiPage("");
	Info.SetHasArgs(false);
	Info.AddType(CModInfo::NetworkModule);
	Info.SetArgsHelpText("Anti Forced Part - Rejoins on /remove, /fpart, etc.");
}



NETWORKMODULEDEFS(CAntiForcePartMod, "Anti Forced Part")