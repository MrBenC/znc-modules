/*
 *	Author: Ben
 *	Contact: Ben @ irc.chatlounge.net
 *
 *	Version: 0.1-Alpha
 *
 *	Description:
 *
 *		This module is designed to attempt to unban you if a channel
 *	ban is set that matches you, and you are already inside the channel.
 *	This particular version is designed only for strictly RFC 1459 IRC
 *	daemons.  It doesn't take into account extended ban types that may
 *	be present in certain IRC daemons.
 *
 *		This module has only been tested on ZNC version 1.4, not any
 *	other versions.  However, it will probably work on 1.2 and 1.5-git.
 *
 *	Installation:
 *
 *		This has only been tested on Debian 7, but should work on other
 *	OSes.
 *
 *		This module requires installing the following library, downloaded
 *	here: http://www.over-yonder.net/~fullermd/projects/libcidr#download
 *
 *		These instructions assume you will be installing it into the
 *	default location.  Prior to running znc-buildmod, the following
 *	environment variables need to be set with the following commands:
 *
 *		export CXXFLAGS=" -lcidr"
 *		export LIBS="-lcidr"
 *		export LDFLAGS="-I/usr/local/include"
 *		export CPPFLAGS="-I/usr/local/include"
 *
 *		Then use znc-buildmod in the regular manner to build the module.
 *
 *	Usage:
 *
 *		NOTE: All commands support wildcards for the #channel argument.
 *	For example, #Te* will match #Test, #temp, and #Testing .  You can
 *	also use "all" for the channel argument for all channels.
 *
 *		To enable automatic unbanning on a channel:
 *
 *	/msg *autounbanrfc1459 enable #channel
 *
 *		To disable automatic unbanning on a channel:
 *
 *	/msg *autounbanrfc1459 disable #channel
 *
 *		To list if it's enabled or disabled on a given channel:
 *
 *	/msg *autounbanrfc1459 status #channel
 *
 *		Sending any other command should generate a message containing
 *	the supported commands.
 *
 *	Requirements/Dependencies/Assumptions (for full functionality):
 *	- ChanServ is available, and is available via the /cs shortcut.
 *	- You have proper access to unban yourself from the channel.
 *	- You remain Opped 24/7.
 *	- The IRCd is a fork of Charybdis.
 *	- Bindhost is set, and not to a wildcard value.
 *	- The ZNC instance is publicly accessible from the Internet,
 *		not behind a NAT.
 *
 *	TODO:
 *	- Add the ability to temporarily Op to lift a ban, to accommodate
 *		the strange "op as needed" behavior.
 *	- Add the ability to change this via WebAdmin.
 *	- Possibly add the ability for an alternative out-of-channel
 *		unban command.
 *
 *
 *	WARNING:
 *
 *		This module should be considered dangerous, as the module
 *	may, in some cases, prevent other Ops from intervening if your
 *	access is somehow compromised or you run a broken script/module.
 *
 *	Limitations:
 *
 *	- This module will function in a reduced state if ChanServ is down.
 *	- This module will not function if you lack appropriate channel access.
 *	- This module will not prevent your access from being removed.
 *	- This module will lift bans that match you coming from ANY source,
 *		including yourself.
 *	- If enabled in a channel where you aren't presently opped, the
 *		module will attempt to send mode commands to the server.  This
 *		may result it a flood of notices that you aren't a channel
 *		operator, or if you have enabled oper override, a flood of server
 *		notices.
 *
 *	Change Log:
 *
 *	August 28th, 2014 (version 0.1-Alpha):
 *	- Adapted from AutoUnbanChary to only include the RFC1459 hostmask
 *	  bans.
 *
 *
 *
 */

#include <znc/Client.h>
#include <znc/Chan.h>
#include <znc/Modules.h>
#include <znc/IRCNetwork.h>
#include <znc/IRCSock.h>
#include <map>
#include <string.h>

using namespace std;

extern "C" {
	#include <libcidr.h>
}

class CAutoUnbanChanMod : public CModule {
public:
	CString ident;
	CString hostname;
	CString NickIdent;
	
	CString hostmask;
	CString hostmaskIP;
	
	// CIDR Objects for ban comparisons.
	CIDR* bigcidr;
	CIDR* littlecidr;
	
	// ZNC doesn't have a built-in banlist tracker, so
	// create a multimap to store it.
	std::multimap <CString, CString> channelBans;

	MODCONSTRUCTOR(CAutoUnbanChanMod) {}

	virtual ~CAutoUnbanChanMod() {
		channelBans.clear();
	}

	// Function to add to the ban list.  Used because it checks
	// if the ban is already in the list.
	void AddToBanList(CString& channel, CString& banmask) {
		channel.MakeLower();
		banmask.MakeLower();
		
		bool exists = false;
		
		pair<multimap<CString,CString>::iterator, multimap<CString,CString>::iterator> ppp;
		ppp = channelBans.equal_range(channel);
		
		for (multimap<CString, CString>::iterator it = ppp.first; it != ppp.second; ++it) {
			if ((*it).second == banmask) {
				exists = true;
			};
		};
		
		if (exists == false) {
			channelBans.insert(pair<CString, CString>(channel, banmask));
		};
	};

	// May be used in several places, so a method to remove from the ban list.
	void DelFromBanList(CString& channel, CString& banmask) {
		channel.MakeLower();
		banmask.MakeLower();
		
		pair<multimap<CString,CString>::iterator, multimap<CString,CString>::iterator> ppp;
		ppp = channelBans.equal_range(channel);
		
		for (multimap<CString, CString>::iterator it = ppp.first; it != ppp.second; ++it) {
			if ((*it).second == banmask) {
				channelBans.erase(it);
				break;
			};
		};	
	};
	
	// Clears the ban list for a channel.  Called when the user parts or is kicked from a channel.
	void ClearChannelFromBanList(CString& channel) {
		channel.MakeLower();
		
		channelBans.erase(channel);
	};
	
	// OnRaw hook to capture relevant data.
	virtual EModRet OnRaw(CString& sLine) {
		
		// Parse self whois, get banmask and realname.
		if (sLine.Token(1) == "311" && sLine.Token(3) == m_pNetwork->GetIRCNick().GetNick()) {
			hostname = sLine.Token(5);
			hostname.MakeLower();
			hostmask = sLine.Token(3) + "!" + sLine.Token(4) + "@" + sLine.Token(5);
			hostmask.MakeLower();
			hostmaskIP = sLine.Token(3) + "!" + sLine.Token(4) + "@" + m_pNetwork->GetBindHost();
			hostmaskIP.MakeLower();
			
			ident = sLine.Token(4);
			ident.MakeLower();
			
			NickIdent = sLine.Token(3) + "!" + sLine.Token(4);
			NickIdent.MakeLower();
		}

		// Catch the raw numeric to add to the ban list.
		if (sLine.Token(1) == "367") {
			CString channelname = sLine.Token(3);
			CString banmask = sLine.Token(4);
			
			AddToBanList(channelname, banmask);
		};
		
		// Update the stored hostname, when umode +x or +h is changed.
		if (sLine.Token(1) == "396") {
			hostname = sLine.Token(3);
		}

		// Perform an unban from outside of the channel, using ChanServ if a channel can't be joined.
		if (sLine.Token(1) == "474") {
			CString sChannel = sLine.Token(3);
			
			PutIRC("cs unban " + sChannel + " " + m_pNetwork->GetIRCNick().GetNick());
			PutIRC("JOIN " + sChannel);
		}
		
		return CONTINUE;
	}
	
	// Hook to catch /msg commands to the module.
	virtual void OnModCommand(const CString& sCommand) {
		CString sCmdName = sCommand.Token(0);
		CString sChannel = sCommand.Token(1);
		
		sCmdName.MakeLower();
		sChannel.MakeLower();

		if (sCmdName == "getbanlist") {
			if (sCommand.Token(1) == "") {
				PutModule("Syntax: GetBanList #channel");
			} else {
				PutModule("Ban list for " + sCommand.Token(1) + ":");
				pair< multimap<CString, CString>::iterator, multimap<CString, CString>::iterator> ppp;
				
				ppp = channelBans.equal_range(sChannel);
				
				for (multimap<CString, CString>::iterator it2 = ppp.first; it2 != ppp.second; ++it2)
				{
					PutModule((*it2).first + " " + (*it2).second);
				};
			};
		}
		else if (sCmdName == "enable") {
			if (sCommand.Token(1).AsLower() == "all") {
				const vector<CChan*>& vChans = m_pNetwork->GetChans();
				for (unsigned int i = 0; i < vChans.size(); i++ ) {
					const CChan* pChan = vChans[i];
					SetNV(pChan->GetName().AsLower(),"1");
				};
				PutModule("Enabled automatic unban on all channels on this network.");
			} else if (!sCommand.Token(1).AsLower().empty()) {
				const vector<CChan*>& vChans = m_pNetwork->GetChans();
				for (unsigned int i = 0; i < vChans.size(); i++ ) {
					const CChan* pChan = vChans[i];

					CString state = GetNV(pChan->GetName().AsLower());
					if ((GetNV(pChan->GetName().AsLower()) != "1") &&
						(pChan->GetName().AsLower().WildCmp(sCommand.Token(1).AsLower())))
					{
						SetNV(pChan->GetName().AsLower(),"1");
						PutModule("Enabled automatic unban on: " + pChan->GetName());
					};
				};
			} else {
				PutModule("Syntax: enable #channel | all");
			};
		}
		else if (sCmdName == "disable") {
			if (sCommand.Token(1).AsLower() == "all") {
				const vector<CChan*>& vChans = m_pNetwork->GetChans();
				for (unsigned int i = 0; i < vChans.size(); i++ ) {
					const CChan* pChan = vChans[i];
					MCString::iterator it = FindNV(pChan->GetName().AsLower());
					if (it != EndNV())
						DelNV(it);
				};
				PutModule("Disabled automatic unban on all channels on this network.");
			} else if (!sCommand.Token(1).AsLower().empty()) {
				const vector<CChan*>& vChans = m_pNetwork->GetChans();
				for (unsigned int i = 0; i < vChans.size(); i++ ) {
					const CChan* pChan = vChans[i];

					CString state = GetNV(pChan->GetName().AsLower());
					if ((GetNV(pChan->GetName().AsLower()) == "1") &&
						(pChan->GetName().AsLower().WildCmp(sCommand.Token(1).AsLower())))
					{
						MCString::iterator it = FindNV(pChan->GetName().AsLower());
						if (it != EndNV())
							DelNV(it);

						PutModule("Disabled automatic unban on: " + pChan->GetName());
					};
				};
			} else {
				PutModule("Syntax: disable #channel | all");
			};
		}
		else if (sCmdName == "status") {
			if (sCommand.Token(1).AsLower() == "all") {
				const vector<CChan*>& vChans = m_pNetwork->GetChans();
				for (unsigned int i = 0; i < vChans.size(); i++ ) {
					const CChan* pChan = vChans[i];

					CString state = GetNV(pChan->GetName().AsLower());
					if (state == "1") {
						PutModule("Automatic unban is currently ENABLED on " + pChan->GetName() + ".");
					}
					else {
						PutModule("Automatic unban is currently DISABLED on " + pChan->GetName() + ".");
					}
				};
			} else if (!sCommand.Token(1).AsLower().empty()) {
				const vector<CChan*>& vChans = m_pNetwork->GetChans();
				for (unsigned int i = 0; i < vChans.size(); i++ ) {
					const CChan* pChan = vChans[i];

					CString state = GetNV(pChan->GetName().AsLower());
					if (pChan->GetName().AsLower().WildCmp(sCommand.Token(1).AsLower())) {
						if (state == "1") {
							PutModule("Automatic unban is currently ENABLED on " + pChan->GetName() + ".");
						}
						else {
							PutModule("Automatic unban is currently DISABLED on " + pChan->GetName() + ".");
						}
					};
				};
			} else {
				PutModule("Syntax: status #channel | all");
			};
		}
		else {
			PutModule("Commands: getbanlist, disable, enable, status");
		};
	}

	// Run a self-whois on connection to the IRC server to generate the needed numerics.
	virtual void OnIRCConnected() {
		PutIRC("WHOIS " + m_pNetwork->GetIRCNick().GetNick() + " " + m_pNetwork->GetIRCNick().GetNick());
	}
	
	// Function to process standard bans that everyone is familiar with.
	void OnModeAddedRFC1459(const CNick& OpNick, CChan& Channel, char uMode, const CString& sArgs, bool bAdded, bool bNoChange) {
		// Check it's a ban and that it was added, not removed.
		// Check hostmask first since RDNS usually succeed, and most bans are based on this.  Then check IP based bans, since
		// can apply even if RDNS succeeds (on most IRCds).  And RDNS lookups could fail on the next connection.

		CString sArgs2 = sArgs.Replace_n("@", " ");
		sArgs2.MakeLower();
		
		if (hostmask.WildCmp(sArgs.AsLower()) || hostmaskIP.WildCmp(sArgs.AsLower())) {
			PutModule("Ban mask matches hostmask.");
			PutIRC("MODE " + Channel.GetName() + " -" + uMode + " " + sArgs);
		} else if (NickIdent.WildCmp(sArgs2.Token(0)) &&
			cidrmatches(sArgs2.Token(1), m_pNetwork->GetBindHost())) {
			
			// While CIDR based bans are uncommon, they might be used.
			// PutModule("Ban CIDR mask matches hostmask."); // - DEBUG Junk
			PutIRC("MODE " + Channel.GetName() + " -" + uMode + " " + sArgs);
		}
	}
	
	// Check if the CIDR mask matches.  Banmask is "big," host IP is "little."
	bool cidrmatches(const CString& big, const CString& little) {
	
		//CIDR*
		bigcidr = cidr_from_str(big.c_str());
		//CIDR*
		littlecidr = cidr_from_str(little.c_str());
		
		if (cidr_contains(bigcidr, littlecidr) == 0) {
			return true;
		} else {
			return false;
		}
	};
	
	virtual void OnMode(const CNick& OpNick, CChan& Channel, char uMode, const CString& sArgs, bool bAdded, bool bNoChange) {
		if (GetNV(Channel.GetName().AsLower()) == "1") {
			if (uMode == 'b') {
				CString channelname = Channel.GetName();
				CString modeArgs = sArgs;				

				if (bAdded) {
					AddToBanList(channelname, modeArgs);

					OnModeAddedRFC1459(OpNick, Channel, uMode, sArgs, bAdded, bNoChange);
				} else {
					DelFromBanList(channelname, modeArgs);
				};
			};
		};
	};
	
	// Get the ban and quiet lists when the channel is joined.
	virtual void OnJoin(const CNick& Nick, CChan& Channel) {
		if (Nick.GetNick() == m_pNetwork->GetIRCNick().GetNick()) {
			PutIRC("MODE " + Channel.GetName() + " b");
			PutIRC("MODE " + Channel.GetName() + " q");
		}
	};
	
	// Clear the record of the list bans and quiets for the channel when leaving it.
	virtual void OnPart(const CNick& Nick, CChan& Channel, const CString& sMessage) {
		if (Nick.GetNick() == m_pNetwork->GetIRCNick().GetNick()) {
			CString channel = Channel.GetName();
			channel.MakeLower();
			
			ClearChannelFromBanList(channel);
		};
	};
	
	// Ditto for OnPart, except for kicks.
	virtual void OnKick(const CNick& OpNick, const CString& sKickedNick, CChan& Channel, const CString& sMessage) {
		if (sKickedNick == m_pNetwork->GetIRCNick().GetNick()) {
			CString channel = Channel.GetName();
			channel.MakeLower();
			
			ClearChannelFromBanList(channel);
		};	
	};

};

template<> void TModInfo<CAutoUnbanChanMod>(CModInfo& Info) {
        Info.SetWikiPage("");
        Info.SetHasArgs(false);
        Info.AddType(CModInfo::NetworkModule);
        Info.SetArgsHelpText("Automatically unbans you from a channel if you banned in it.");
}



NETWORKMODULEDEFS(CAutoUnbanChanMod, "Automatic channel unbanner if you banned in it.")
