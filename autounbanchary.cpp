/*
 *	Author: Ben
 *	Contact: Ben @ irc.chatlounge.net
 *
 *	Version: 0.1-Alpha
 *
 *	Description:
 *
 *		This module is designed to attempt to unban you if a channel
 *	ban is set that matches you, and you are already inside the channel.
 *	This particular version is designed for the Charybdis IRCd daemon
 *	and its forks, including ShadowIRCd, Ircd-seven, and Elemental-IRCd.
 *	It will likely NOT work to it's full potential if the network lacks
 *	ChanServ, or if loaded on a network with a different IRCd.
 *
 *		This module has only been tested on ZNC version 1.4, not any
 *	other versions.  However, it will probably work on 1.2 and 1.5-git.
 *
 *	Installation:
 *
 *		This has only been tested on Debian 7, but should work on other
 *	OSes.
 *
 *		This module requires installing the following library, downloaded
 *	here: http://www.over-yonder.net/~fullermd/projects/libcidr#download
 *
 *		These instructions assume you will be installing it into the
 *	default location.  Prior to running znc-buildmod, the following
 *	environment variables need to be set with the following commands:
 *
export CXXFLAGS=" -lcidr"
export LIBS="-lcidr"
export LDFLAGS="-I/usr/local/include"
export CPPFLAGS="-I/usr/local/include"
 *
 *		Then use znc-buildmod in the regular manner to build the module.
 *
 *	Usage:
 *
 *		NOTE: All commands support wildcards for the #channel argument.
 *	For example, #Te* will match #Test, #temp, and #Testing .  You can
 *	also use "all" for the channel argument for all channels.
 *
 *		To enable automatic unbanning on a channel:
 *
 *	/msg *autounbanchary enable #channel
 *
 *		To disable automatic unbanning on a channel:
 *
 *	/msg *autounbanchary disable #channel
 *
 *		To list if it's enabled or disabled on a given channel:
 *
 *	/msg *autounbanchary status #channel
 *
 *		Sending any other command should generate a message containing
 *	the supported commands.
 *
 *	Requirements/Dependencies/Assumptions (for full functionality):
 *	- ChanServ is available, and is available via the /cs shortcut.
 *	- You have proper access to unban yourself from the channel.
 *	- You remain Opped 24/7.
 *	- The IRCd is a fork of Charybdis.
 *	- Bindhost is set, and not to a wildcard value.
 *	- The ZNC instance is publicly accessible from the Internet,
 *		not behind a NAT.
 *
 *	TODO:
 *	- Add the ability to temporarily Op to lift a ban, to accommodate
 *		the strange "op as needed" behavior.
 *	- Add the ability to change this via WebAdmin.
 *	- Possibly add the ability for an alternative out-of-channel
 *		unban command.
 *  - Don't bother to unban if a matching +e is set.
 *    - If -e, check to see if a matching +e is still set.
 *    - If not, remove all matching bans.
 *
 *
 *	WARNING:
 *
 *		This module should be considered dangerous, as the module
 *	may, in some cases, prevent other Ops from intervening if your
 *	access is somehow compromised or you run a broken script/module.
 *
 *	Limitations:
 *
 *	- This module will function in a reduced state if ChanServ is down.
 *	- This module will not function if you lack appropriate channel access.
 *	- This module will not prevent your access from being removed.
 *	- This module will lift bans that match you coming from ANY source,
 *		including yourself.
 *	- If enabled in a channel where you aren't presently opped, the
 *		module will attempt to send mode commands to the server.  This
 *		may result it a flood of notices that you aren't a channel
 *		operator, or if you have enabled oper override, a flood of server
 *		notices.
 *	- The module assumes NickServ is available, it's called "NickServ",
 *		and Atheme or Anope services are used.  Otherwise, you will
 *		have to /whois yourself whenever you change your services
 *		account name.
 *
 *	Change Log:
 *
 *	August 15th, 2014 (version 0.1-Alpha):
 *	- Initial code.
 *  November 7th, 2015
 *  - Fixed $x matching logic
 *  - Cleanups
 *  - Improved description.
 *
 *
 */

#include <znc/Client.h>
#include <znc/Chan.h>
#include <znc/Modules.h>
#include <znc/IRCNetwork.h>
#include <znc/IRCSock.h>
#include <map>
#include <string.h>

using namespace std;

extern "C" {
	#include <libcidr.h>
}

class CAutoUnbanChary : public CModule {
public:
	CString realname;

	CString accountname;

	CString ident;
	CString hostname;
	CString NickIdent;

	CString hostmask;
	CString hostmaskIP;

	CString ircservername;

	/* CIDR Objects for ban comparisons. */
	CIDR* bigcidr;
	CIDR* littlecidr;

	/* ZNC doesn't have a built-in banlist tracker, so
	 * create a multimap to store it.
	 */
	std::multimap <CString, CString> channelBans;

	/* Same for the quiet list in Charybdis forks. */
	std::multimap <CString, CString> channelQuiets;

	/* Same for the except list. */
	std::multimap <CString, CString> channelExcepts;

	MODCONSTRUCTOR(CAutoUnbanChary) {}

	virtual ~CAutoUnbanChary() {
		channelBans.clear();
		channelQuiets.clear();
		channelExcepts.clear();
	}

	/* Function to add to the ban list.  Used because it checks
	 * if the ban is already in the list.
	 */
	void AddToBanList(CString& channel, CString& banmask) {
		channel.MakeLower();
		banmask.MakeLower();
		
		bool exists = false;
		
		pair<multimap<CString,CString>::iterator, multimap<CString,CString>::iterator> ppp;
		ppp = channelBans.equal_range(channel);
		
		for (multimap<CString, CString>::iterator it = ppp.first; it != ppp.second; ++it) {
			if ((*it).second == banmask) {
				exists = true;
			};
		};
		
		if (exists == false) {
			channelBans.insert(pair<CString, CString>(channel, banmask));
		};
	};

	/* Same as AddToBanList() for the quiet list. */
	void AddToQuietList(CString& channel, CString& banmask) {
		channel.MakeLower();
		banmask.MakeLower();
	
		bool exists = false;
		
		pair<multimap<CString,CString>::iterator, multimap<CString,CString>::iterator> ppp;
		ppp = channelQuiets.equal_range(channel);
		
		for (multimap<CString, CString>::iterator it = ppp.first; it != ppp.second; ++it) {
			if ((*it).second == banmask) {
				exists = true;
			};
		};
		
		if (exists == false) {
			channelQuiets.insert(pair<CString, CString>(channel, banmask));
		};
	};

	/* May be used in several places, so a function to remove from the ban list. */
	void DelFromBanList(CString& channel, CString& banmask) {
		channel.MakeLower();
		banmask.MakeLower();
		
		pair<multimap<CString,CString>::iterator, multimap<CString,CString>::iterator> ppp;
		ppp = channelBans.equal_range(channel);
		
		for (multimap<CString, CString>::iterator it = ppp.first; it != ppp.second; ++it) {
			if ((*it).second == banmask) {
				channelBans.erase(it);
				break;
			};
		};	
	};

	/* Same thing, but for the quiet list. */
	void DelFromQuietList(CString& channel, CString& banmask) {
		channel.MakeLower();
		banmask.MakeLower();

		pair<multimap<CString,CString>::iterator, multimap<CString,CString>::iterator> ppp;
		ppp = channelQuiets.equal_range(channel);
		
		for (multimap<CString, CString>::iterator it = ppp.first; it != ppp.second; ++it) {
			if ((*it).second == banmask) {
				channelQuiets.erase(it);
				break;
			};
		};	
	};
	
	/* Clears the ban list for a channel.  Called when the user parts or is kicked from a channel. */
	void ClearChannelFromBanList(CString& channel) {
		channel.MakeLower();
		
		channelBans.erase(channel);
	};

	/* Clears the quiet list for a channel.  Called when the user parts or is kicked from a channel. */
	void ClearChannelFromQuietList(CString& channel) {
		channel.MakeLower();
		
		channelQuiets.erase(channel);
	};

	/* Various methods for handling each type of extban.  Each return true if the ban mask matches the user.
	 * Listed in alphabetical order.
	 */
	
	/* Function to handle extban $a - Services account name ban. */
	bool IsBannedExtbanA(CString& sArgs) {
		CString accountnameban = "$a:" + accountname;
		accountnameban.MakeLower();
		
		CString sArg = sArgs.AsLower();
		
		if (accountnameban.WildCmp(sArg)) {
			return true;
		};
		
		return false;
	};
	
	/* Function to handle extban $c - Is the user joined in the specified channel? */
	bool IsBannedExtbanC(CString& sArgs) {
		const std::vector<CChan*>& vChans = m_pNetwork->GetChans();
		for (unsigned int i = 0; i < vChans.size(); i++ ) {
			const CChan* pChan = vChans[i];
			if (sArgs.TrimLeft_n("$c:").AsLower() == pChan->GetName().AsLower()) {
				return true;
			}
		}
		
		return false;
	};
	
	/* Function to handle extban $m - Usermode ban. */
	bool IsBannedExtbanM(CString& sArgs) {
		CString extbanUModes = sArgs.TrimLeft_n("$m:");

		/* Get the current user modes of the user. */
		CIRCSock *pIRCSock = m_pNetwork->GetIRCSock();
		std::set<unsigned char> userModes = pIRCSock->GetUserModes();
		CString uModes = "";

		for (std::set<unsigned char>::const_iterator it = userModes.begin(); it != userModes.end(); ++it) {
				uModes += *it;
		};

		while (extbanUModes != "") {
			if (uModes.WildCmp("*" + extbanUModes.Left(1) + "*")) {
				extbanUModes = "";
				return true;
			};
			
			extbanUModes.LeftChomp();
		};
		
		return false;
	};
	
	/* Function to handle extban $o - Oper ban. */
	bool IsBannedExtbanO(CString& sArgs) {
		/* Get the current user modes of the user. */
		CIRCSock *pIRCSock = m_pNetwork->GetIRCSock();
		std::set<unsigned char> userModes = pIRCSock->GetUserModes();
		CString uModes = "";

		for (std::set<unsigned char>::const_iterator it = userModes.begin(); it != userModes.end(); ++it) {
				uModes += *it;
		};
		
		/* Opers have umodes o, a, and/or O. */
		if (uModes.WildCmp("*o*") || uModes.WildCmp("*a*") || uModes.WildCmp("*O*")) {
			return true;
		};
		
		return false;
	};
	
	/* Function to handle extban $r - Realname, or GECOS field ban. */
	bool IsBannedExtbanR(CString& sArgs) {
		CString realnameban = "$r:" + m_pNetwork->GetRealName();
		realnameban.MakeLower();
		
		CString sArg = sArgs.AsLower();
	
		if (realnameban.WildCmp(sArg)) {
			return true;
		}
		
		return false;
	};
	
	/* Function to handle extban $s - Matches users on a specific server. */
	bool IsBannedExtbanS(CString& sArgs) {
		if (ircservername.WildCmp(sArgs.TrimLeft_n("$s:").AsLower())) {
			return true;
		}
		
		return false;
	};
	
	/* Function to handle extban $x - n!u@h#realname */
	bool IsBannedExtbanX(CString& sArgs) {

		/* $x is case insensitive, so convert it all to lower case. */
		CString sArgs2 = sArgs.TrimLeft_n("$x:");
		sArgs2.MakeLower();

		/* Create a string to contain a perfectly matching extban %x to compare the Args against.
		 * Convert it to lower case as well.
		 */
		CString extbanXtemp = hostmask + "#" + m_pNetwork->GetRealName();
		extbanXtemp.MakeLower();
		
		if(extbanXtemp.WildCmp(sArgs2))
			return true;
		
		return false;
	};
	
	/* Function to handle extban $z - Matches users who are using SSL. */
	bool IsBannedExtbanZ(CString& sArgs) {
		// Get the current user modes of the user.
		CIRCSock *pIRCSock = m_pNetwork->GetIRCSock();
		std::set<unsigned char> userModes = pIRCSock->GetUserModes();
		CString uModes = "";

		for (std::set<unsigned char>::const_iterator it = userModes.begin(); it != userModes.end(); ++it) {
				uModes += *it;
		};
		
		/* SSL users have umode Z. */
		if (uModes.WildCmp("*Z*")) {
			return true;
		};
		
		return false;
	};
	
	/* FindMatch - Calls all the Extban matching functions in an attempt to find a positive match.
	 * $j is skipped, since it only goes one "level" deep.
	 */
	bool FindMatch(CString& sArgs) {
		/* Extban $a - Account name. */
		if (sArgs.WildCmp("$a:*")) {
			if(IsBannedExtbanA(sArgs)) {
				return true;
			};
		}
		/* Extban $r - Real name. */
		else if (sArgs.WildCmp("$r:*")) {
			if(IsBannedExtbanR(sArgs)) {
				return true;
			};
		}
		/* Extban $x - nick!ident@host#realname */
		else if (sArgs.WildCmp("$x:*")) {
			if (IsBannedExtbanX(sArgs)) {
				return true;
			};
		}
		/* $c channel membership extban */
		else if (sArgs.WildCmp("$c:*")) {
			if (IsBannedExtbanC(sArgs)) {
				return true;
			};
		}
		/* Silly usermode based extban. */
		else if (sArgs.WildCmp("$m:*")) {
			if (IsBannedExtbanM(sArgs)) {
				return true;
			};
		}
		/* Silly oper extban. */
		else if (sArgs.WildCmp("$o")) {
			if (IsBannedExtbanO(sArgs)) {
				return true;
			};
		}
		/* Check for the server based extban. */
		else if (sArgs.WildCmp("$s:*")) {
			if (IsBannedExtbanS(sArgs)) {
				return true;
			};
		}
		/* Remove bans on SSL users (is anyone really going to set such a ban/quiet?!) */
		else if (sArgs.WildCmp("$z")) {
			if (IsBannedExtbanZ(sArgs)) {
				return true;
			};
		};
		
		return false;
	};
	
	/* OnRaw hook to capture relevant data. */
	virtual EModRet OnRaw(CString& sLine) {
		
		/* Parse self whois, get banmask and realname. */
		if (sLine.Token(1) == "311" && sLine.Token(3) == m_pNetwork->GetIRCNick().GetNick()) {
			hostname = sLine.Token(5);
			hostname.MakeLower();
			hostmask = sLine.Token(3) + "!" + sLine.Token(4) + "@" + sLine.Token(5);
			hostmask.MakeLower();
			hostmaskIP = sLine.Token(3) + "!" + sLine.Token(4) + "@" + m_pNetwork->GetBindHost();
			hostmaskIP.MakeLower();
			
			realname = m_pNetwork->GetRealName();
			realname.MakeLower();
			
			ident = sLine.Token(4);
			ident.MakeLower();
			
			NickIdent = sLine.Token(3) + "!" + sLine.Token(4);
			NickIdent.MakeLower();

			return CONTINUE;
		}
		/* Parse self whois, get current server. */
		else if (sLine.Token(1) == "312" && sLine.Token(3) == m_pNetwork->GetIRCNick().GetNick()) {
			ircservername = sLine.Token(4).AsLower();
			return CONTINUE;
		}
		/* If on /whois, numeric 330 exists, the user is logged into services. */
		else if (sLine.Token(1) == "330" && sLine.Token(3) == m_pNetwork->GetIRCNick().GetNick()) {
			accountname = sLine.Token(4);
			return CONTINUE;
		}
		/* Catch the raw numeric to add to the invite list. */
		else if (sLine.Token(1) == "348" && sLine.Token(3) == m_pNetwork->GetIRCNick().GetNick()) {
			CString channelname = sLine.Token(3);
			CString banmask = sLine.Token(4);

			return CONTINUE;
		}
		/* Catch the raw numeric to add to the ban list. */
		else if (sLine.Token(1) == "367") {
			CString channelname = sLine.Token(3);
			CString banmask = sLine.Token(4);

			AddToBanList(channelname, banmask);
			return CONTINUE;
		}
		/* Update the stored hostname, when umode +x or +h is changed. */
		else if (sLine.Token(1) == "396") {
			hostname = sLine.Token(3);
			return CONTINUE;
		}
		/* Perform an unban from outside of the channel, using ChanServ if a channel can't be joined. */
		else if (sLine.Token(1) == "474") {
			CString sChannel = sLine.Token(3);

			PutIRC("cs unban " + sChannel + " " + m_pNetwork->GetIRCNick().GetNick());
			PutIRC("JOIN " + sChannel);
			return CONTINUE;
		}
		/* Catch the raw numeric to add to the quiets list. */
		else if (sLine.Token(1) == "728") {
			CString channelname = sLine.Token(3);
			CString banmask = sLine.Token(4);

			AddToQuietList(channelname, banmask);
			return CONTINUE;
		};

		return CONTINUE;
	}
	
	/* Hook to catch /msg commands to the module. */
	virtual void OnModCommand(const CString& sCommand) {
		CString sCmdName = sCommand.Token(0);
		CString sChannel = sCommand.Token(1);
		
		sCmdName.MakeLower();
		sChannel.MakeLower();

		if (sCmdName == "getbanlist") {
			if (sCommand.Token(1) == "") {
				PutModule("Syntax: GetBanList #channel");
			} else {
				PutModule("Ban list for " + sCommand.Token(1) + ":");
				pair< multimap<CString, CString>::iterator, multimap<CString, CString>::iterator> ppp;
				
				ppp = channelBans.equal_range(sChannel);
				
				for (multimap<CString, CString>::iterator it2 = ppp.first; it2 != ppp.second; ++it2)
				{
					PutModule((*it2).first + " " + (*it2).second);
				};
			};
		}
		else if (sCmdName == "getquietlist") {
			if (sCommand.Token(1) == "") {
				PutModule("Syntax: GetQuietList #channel");
			} else {
				PutModule("Quiet list for " + sCommand.Token(1) + ":");
				pair< multimap<CString, CString>::iterator, multimap<CString, CString>::iterator> ppp;
				
				ppp = channelQuiets.equal_range(sChannel);
				
				for (multimap<CString, CString>::iterator it2 = ppp.first; it2 != ppp.second; ++it2)
				{
					PutModule((*it2).first + " " + (*it2).second);
				};
			};
		}
		else if (sCmdName == "getexceptlist") {
			if (sCommand.Token(1) == "") {
				PutModule("Syntax: GetExceptList #channel");
			} else {
				PutModule("Ban Exception list for " + sCommand.Token(1) + ":");
				pair< multimap<CString, CString>::iterator, multimap<CString, CString>::iterator> ppp;
				
				ppp = channelExcepts.equal_range(sChannel);
				
				for (multimap<CString, CString>::iterator it2 = ppp.first; it2 != ppp.second; ++it2)
				{
					PutModule((*it2).first + " " + (*it2).second);
				};
			};
		}
		else if (sCmdName == "enable") {
			if (sCommand.Token(1).AsLower() == "all") {
				const vector<CChan*>& vChans = m_pNetwork->GetChans();
				for (unsigned int i = 0; i < vChans.size(); i++ ) {
					const CChan* pChan = vChans[i];
					SetNV(pChan->GetName().AsLower(),"1");
				};
				PutModule("Enabled automatic unban on all channels on this network.");
			} else if (!sCommand.Token(1).AsLower().empty()) {
				const vector<CChan*>& vChans = m_pNetwork->GetChans();
				for (unsigned int i = 0; i < vChans.size(); i++ ) {
					const CChan* pChan = vChans[i];

					CString state = GetNV(pChan->GetName().AsLower());
					if ((GetNV(pChan->GetName().AsLower()) != "1") &&
						(pChan->GetName().AsLower().WildCmp(sCommand.Token(1).AsLower())))
					{
						SetNV(pChan->GetName().AsLower(),"1");
						PutModule("Enabled automatic unban on: " + pChan->GetName());
					};
				};
			} else {
				PutModule("Syntax: enable #channel | all");
			};
		}
		else if (sCmdName == "disable") {
			if (sCommand.Token(1).AsLower() == "all") {
				const vector<CChan*>& vChans = m_pNetwork->GetChans();
				for (unsigned int i = 0; i < vChans.size(); i++ ) {
					const CChan* pChan = vChans[i];
					MCString::iterator it = FindNV(pChan->GetName().AsLower());
					if (it != EndNV())
						DelNV(it);
				};
				PutModule("Disabled automatic unban on all channels on this network.");
			} else if (!sCommand.Token(1).AsLower().empty()) {
				const vector<CChan*>& vChans = m_pNetwork->GetChans();
				for (unsigned int i = 0; i < vChans.size(); i++ ) {
					const CChan* pChan = vChans[i];

					CString state = GetNV(pChan->GetName().AsLower());
					if ((GetNV(pChan->GetName().AsLower()) == "1") &&
						(pChan->GetName().AsLower().WildCmp(sCommand.Token(1).AsLower())))
					{
						MCString::iterator it = FindNV(pChan->GetName().AsLower());
						if (it != EndNV())
							DelNV(it);

						PutModule("Disabled automatic unban on: " + pChan->GetName());
					};
				};
			} else {
				PutModule("Syntax: disable #channel | all");
			};
		}
		else if (sCmdName == "status") {
			if (sCommand.Token(1).AsLower() == "all") {
				const vector<CChan*>& vChans = m_pNetwork->GetChans();
				for (unsigned int i = 0; i < vChans.size(); i++ ) {
					const CChan* pChan = vChans[i];

					CString state = GetNV(pChan->GetName().AsLower());
					if (state == "1") {
						PutModule("Automatic unban is currently ENABLED on " + pChan->GetName() + ".");
					}
					else {
						PutModule("Automatic unban is currently DISABLED on " + pChan->GetName() + ".");
					}
				};
			} else if (!sCommand.Token(1).AsLower().empty()) {
				const vector<CChan*>& vChans = m_pNetwork->GetChans();
				for (unsigned int i = 0; i < vChans.size(); i++ ) {
					const CChan* pChan = vChans[i];

					CString state = GetNV(pChan->GetName().AsLower());
					if (pChan->GetName().AsLower().WildCmp(sCommand.Token(1).AsLower())) {
						if (state == "1") {
							PutModule("Automatic unban is currently ENABLED on " + pChan->GetName() + ".");
						}
						else {
							PutModule("Automatic unban is currently DISABLED on " + pChan->GetName() + ".");
						}
					};
				};
			} else {
				PutModule("Syntax: status #channel | all");
			};
		}
		else {
			PutModule("Commands: getbanlist, getquietlist, disable, enable, status");
		};
	}

	/* Run a self-whois on connection to the IRC server or module load to generate the needed numerics. */
	virtual void OnIRCConnected() {
		CString currentnick = m_pNetwork->GetIRCNick().GetNick();
		PutIRC("WHOIS " + currentnick + " " + currentnick);
	}
	
	virtual bool OnLoad(const CString& sArgsi, CString& sMessage) {
		CString currentnick = m_pNetwork->GetIRCNick().GetNick();
		PutIRC("WHOIS " + currentnick + " " + currentnick);
		return true;
	}
	
	/* Catches the private notice from NickServ in the event your account name changes. */
	virtual EModRet OnPrivNotice(CNick& Nick, CString& sMessage) {
		if (Nick.GetNick() == "NickServ") {
			if (sMessage.WildCmp("*Your account name is now set to*") || sMessage.WildCmp("*The new display is now*")) {
				PutIRC("WHOIS " + m_pNetwork->GetIRCNick().GetNick() + " " + m_pNetwork->GetIRCNick().GetNick());
			}
		}
		
		return CONTINUE;
	}
	
	/* Function to remove extended ban-types specific to Charybdis forks. */
	virtual void OnModeAddedChary(CChan& Channel, char cMode, const CString& sArgs) {

		CString channelname = Channel.GetName();
		CString sArg = sArgs;
		
		if (sArgs.WildCmp("$j:*")) {
			CString sourceChannel = sArg.TrimLeft_n("$j:");
			sourceChannel.MakeLower();

			pair< multimap<CString, CString>::iterator, multimap<CString, CString>::iterator> ppp;

			if (cMode == 'b') {
				ppp = channelBans.equal_range(sourceChannel);
			} else if (cMode == 'q') {
				ppp = channelQuiets.equal_range(sourceChannel);
			}

			CString banmask;

			for (multimap<CString, CString>::iterator it2 = ppp.first; it2 != ppp.second; ++it2)
			{
				banmask = (*it2).second;

				if(FindMatch(banmask))
					PutIRC("MODE " + channelname + " -" + cMode + " " + sArgs);
			};
		}
		else if(FindMatch(sArg))
			PutIRC("MODE " + channelname + " -" + cMode + " " + sArgs);
	}
	
	/* Function to process standard bans that everyone is familiar with. */
	void OnModeAddedRFC1459(const CNick& OpNick, CChan& Channel, char uMode, const CString& sArgs, bool bAdded, bool bNoChange) {
		/* Check it's a ban and that it was added, not removed.
		 * Check hostmask first since RDNS usually succeeds, and most bans are based on this.  Then check IP based bans, since
		 * can apply even if RDNS succeeds (on most IRCds).  And RDNS lookups could fail on the next connection.
		 */

		CString sArgs2 = sArgs.Replace_n("@", " ");
		sArgs2.MakeLower();
		
		if (hostmask.WildCmp(sArgs.AsLower()) || hostmaskIP.WildCmp(sArgs.AsLower())) {
			PutModule("Ban mask matches hostmask.");
			PutIRC("MODE " + Channel.GetName() + " -" + uMode + " " + sArgs);
		} else if (NickIdent.WildCmp(sArgs2.Token(0)) &&
			cidrmatches(sArgs2.Token(1), m_pNetwork->GetBindHost())) {
			
			/* While CIDR based bans are uncommon, they might be used. */
			PutIRC("MODE " + Channel.GetName() + " -" + uMode + " " + sArgs);
		}
	}

	/* Check if the CIDR mask matches.  Banmask is "big," host IP is "little." */
	bool cidrmatches(const CString& big, const CString& little) {
		/* CIDR* objects. */
		bigcidr = cidr_from_str(big.c_str());
		littlecidr = cidr_from_str(little.c_str());

		if (cidr_contains(bigcidr, littlecidr) == 0) {
			return true;
		} else {
			return false;
		}
	};
	
	virtual void OnMode(const CNick& OpNick, CChan& Channel, char uMode, const CString& sArgs, bool bAdded, bool bNoChange) {
		CString channelname = Channel.GetName();
		CString modeArgs = sArgs;

		if (GetNV(Channel.GetName().AsLower()) == "1") {
			if (uMode == 'b') {
				if (bAdded) {
					AddToBanList(channelname, modeArgs);

					OnModeAddedRFC1459(OpNick, Channel, uMode, sArgs, bAdded, bNoChange);
					OnModeAddedChary(Channel, uMode, sArgs);
				} else {
					DelFromBanList(channelname, modeArgs);
				};
			} else if (uMode == 'q') {
				if (bAdded) {
					AddToQuietList(channelname, modeArgs);

					OnModeAddedRFC1459(OpNick, Channel, uMode, sArgs, bAdded, bNoChange);
					OnModeAddedChary(Channel, uMode, sArgs);
				} else {
					DelFromQuietList(channelname, modeArgs);
				};
			};
		};
	};
	
	/* Get the ban and quiet lists when the channel is joined. */
	virtual void OnJoin(const CNick& Nick, CChan& Channel) {
		CString channelname = Channel.GetName();
		if (Nick.GetNick() == m_pNetwork->GetIRCNick().GetNick()) {
			PutIRC("MODE " + channelname + " b");
			PutIRC("MODE " + channelname + " q");
		}

		channelname.MakeLower();
	};
	
	/* Clear the record of the list bans and quiets for the channel when leaving it. */
	virtual void OnPart(const CNick& Nick, CChan& Channel, const CString& sMessage) {
		if (Nick.GetNick() == m_pNetwork->GetIRCNick().GetNick()) {
			CString channel = Channel.GetName();
			channel.MakeLower();

			ClearChannelFromBanList(channel);
			ClearChannelFromQuietList(channel);
		};
	};
	
	/* Ditto for OnPart, except for kicks. */
	virtual void OnKick(const CNick& OpNick, const CString& sKickedNick, CChan& Channel, const CString& sMessage) {
		if (sKickedNick == m_pNetwork->GetIRCNick().GetNick()) {
			CString channel = Channel.GetName();
			channel.MakeLower();

			ClearChannelFromBanList(channel);
			ClearChannelFromQuietList(channel);
		};	
	};

};

template<> void TModInfo<CAutoUnbanChary>(CModInfo& Info) {
        Info.SetWikiPage("");
        Info.SetHasArgs(false);
        Info.AddType(CModInfo::NetworkModule);
        Info.SetArgsHelpText("Automatically unbans you from a channel if you banned in it. - Charybdis/ChatIRCd/ircd-seven/Elemental-IRCd/etc. version");
}



NETWORKMODULEDEFS(CAutoUnbanChary, "Automatic channel unbanner if you banned in it.  Charybdis/ChatIRCd/ircd-seven/Elemental-IRCd/etc. version")
