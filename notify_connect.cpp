/*
 *	Modifying Author:	Ben
 *	Contact:			Ben @ irc.chatlounge.net
 *
 *	Description:
 *
 *		This module is a little different from the version included
 *	with ZNC.  The version included with ZNC only shows the username
 *	of the user connecting, and the IP he's connecting from.
 *
 *	Differences:
 *
 * -The network attached to upon connecting, if any, is displayed.
 * -If the user is an Administrator, the notice will say so upon
 * 	connection or disconnection.
 *
 *	Note:
 *
 *		It's not yet possible to show the network the user has
 *	disconnected from.  I have not yet identified a means or
 *	investigated a way of doing so.
 *
 * Copyright (C) 2004-2014 ZNC, see the NOTICE file for details.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <znc/znc.h>
#include <znc/User.h>
#include <znc/IRCNetwork.h>


class CNotifyConnectMod : public CModule {
public:
	MODCONSTRUCTOR(CNotifyConnectMod) {}

	virtual void OnClientLogin() {
		//SendAdmins(m_pUser->GetUserName() + " attached (from " + m_pClient->GetRemoteIP() + ")");
		if (m_pClient->GetFullName().Replace_n("/"," ").Token(1) == "") {
			if (!m_pUser->IsAdmin()) {
				SendAdmins(m_pUser->GetUserName() + " attached (from " + m_pClient->GetRemoteIP() + ")");
			}
			else {
				SendAdmins(m_pUser->GetUserName() + " (Administrator) attached (from " + m_pClient->GetRemoteIP() + ")");
			}
		}
		else {
			if (!m_pUser->IsAdmin()) {
				SendAdmins(m_pUser->GetUserName() + " attached to " + m_pClient->GetFullName().Replace_n("/"," ").Token(1) + " (from " + m_pClient->GetRemoteIP() + ")");
			}
			else {
				SendAdmins(m_pUser->GetUserName() + " (Administrator) attached to " + m_pClient->GetFullName().Replace_n("/"," ").Token(1) + " (from " + m_pClient->GetRemoteIP() + ")");
			}
		}
	}

	virtual void OnClientDisconnect() {
		if (m_pClient->GetFullName().Replace_n("/"," ").Token(1) == "") {
			if (!m_pUser->IsAdmin()) {
				SendAdmins(m_pUser->GetUserName() + " detached (from " + m_pClient->GetRemoteIP() + ")");
			}
			else {
				SendAdmins(m_pUser->GetUserName() + " (Administrator) detached (from " + m_pClient->GetRemoteIP() + ")");
			}
		}
		else {
			if (!m_pUser->IsAdmin()) {
				SendAdmins(m_pUser->GetUserName() + " detached from " + m_pClient->GetFullName().Replace_n("/"," ").Token(1) + " (from " + m_pClient->GetRemoteIP() + ")");
			}
			else {
				SendAdmins(m_pUser->GetUserName() + " (Administrator) detached from " + m_pClient->GetFullName().Replace_n("/"," ").Token(1) + " (from " + m_pClient->GetRemoteIP() + ")");
			}
		}
	}
	

private:
	void SendAdmins(const CString &msg) {
		CZNC::Get().Broadcast(msg, true, NULL, GetClient());
	}
};

template<> void TModInfo<CNotifyConnectMod>(CModInfo& Info) {
	Info.SetWikiPage("notify_connect");
}

GLOBALMODULEDEFS(CNotifyConnectMod, "Notifies all admin users when a client connects or disconnects.")
