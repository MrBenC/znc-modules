/*
 *	Author: Ben
 *	Contact: Ben @ irc.chatlounge.net
 *
 *	Version: 0.3-Alpha
 *
 *	Description:
 *
 *		This module will automatically reop you in any channel where
 *	you get deopped.  It doesn't matter if the deop originates from
 *	another user in the channel, or if they are outside of the channel.
 *
 *		This module has been developed on, and has only been tested
 *	on ZNC 1.2 and 1.4.  It may or may not work on other versions.
 *
 *	Installation:
 *
 *		Install normally like you would any other module.  Don't
 *	forget to load it.
 *
 *	Usage:
 *
 *		To enable it on a channel, type:
 *	/msg *reopondeop enable #channel
 *
 *		To disable:
 *	/msg *reopondeop disable #channel
 *
 *		To check whether it's enabled on a channel:
 *	/msg *reopondeop state #channel
 *
 *	Requirements/Dependencies:
 *
 *	- You must have a registered nick/account on the network.
 *	- You must have op access on the channel you wish to enable this on.
 *	- The network must have ChanServ, and support the /cs shortcut.
 *
 *	TODO:
 *	- Add input sanitizing.
 *	- Add a check to only add channels the user is in.
 *	- Add support for X/Q (low priority)
 *	- Add a "help" command (though currently all functions should be
 *	  self explanatory.
 *	- Permit editing via the admin web page.
 *	- Perhaps add the ability to add exemptions for source nicks.
 *
 *	WARNING:
 *
 *		This module should be considered highly dangerous.  If you
 *	have another broken script, most other channel ops may be unable to
 *	intervene.
 *
 *	Limitations:
 *
 *	- This module will not function if ChanServ is down.
 *	- This module will not function if you lack appropriate channel access.
 *	- This module will not prevent your access from being removed.
 *	- This module does not support halfop, owner, and protect at this time.
 *	- The only exception to the reop is if you do a direct self deop
 *		(i.e. via /mode #channel -o YourNick).  A deop from any other source,
 *		including ChanServ or OperServ, will trigger the automatic reop.
 *
 *	Change Log:
 *
 *	August 14th, 2014: (version 0.3-Alpha)
 *	- Added the "all" option to the various commands to set and unset
 *	  on every channel on the network.
 *	- Fixed a typo in the help message.
 *
 *	April 20th, 2014: (version 0.2-Alpha):
 *	- Fixed an issue where erroneous notifications may take place.
 *
 *	April 18th, 2014 (version 0.1-Alpha):
 *	- Initial code.
 */

#include <znc/Client.h>
#include <znc/Chan.h>
#include <znc/Modules.h>
#include <znc/IRCNetwork.h>

using std::vector;

class CReOpOnDeOpMod : public CModule {
public:
	MODCONSTRUCTOR(CReOpOnDeOpMod) {}

	virtual ~CReOpOnDeOpMod() {
	}

	virtual void OnModCommand(const CString& sCommand) {
		CString sCmdName = sCommand.Token(0);
		CString sChannel = sCommand.Token(1);

		sCmdName.MakeLower();
		sChannel.MakeLower();

		if ((sCmdName == "enable") && (!sChannel.empty())) {
			if (sCommand.Token(1).AsLower() == "all") {
				const vector<CChan*>& vChans = m_pNetwork->GetChans();
				for (unsigned int i = 0; i < vChans.size(); i++ ) {
					const CChan* pChan = vChans[i];
					SetNV(pChan->GetName().AsLower(),"1");
				};
				PutModule("Enabled Automatic Reop on all channels on this network.");
			}
			else {
				const vector<CChan*>& vChans = m_pNetwork->GetChans();
				for (unsigned int i = 0; i < vChans.size(); i++ ) {
					const CChan* pChan = vChans[i];

					CString state = GetNV(pChan->GetName().AsLower());
					if ((GetNV(pChan->GetName().AsLower()) != "1") &&
						(pChan->GetName().AsLower().WildCmp(sCommand.Token(1).AsLower())))
					{
						SetNV(pChan->GetName().AsLower(),"1");
						PutModule("Enabled automatic reop on deop on: " + pChan->GetName());
					};
				};
			};
		}
		else if ((sCmdName == "disable") && (!sChannel.empty())) {
			if (sCommand.Token(1).AsLower() == "all") {
				const vector<CChan*>& vChans = m_pNetwork->GetChans();
				for (unsigned int i = 0; i < vChans.size(); i++ ) {
					const CChan* pChan = vChans[i];
					MCString::iterator it = FindNV(pChan->GetName().AsLower());
					if (it != EndNV())
						DelNV(it);
				};
				PutModule("Disabled Automatic reop on all channels on this network.");
			}			
			else {
				const vector<CChan*>& vChans = m_pNetwork->GetChans();
				for (unsigned int i = 0; i < vChans.size(); i++ ) {
					const CChan* pChan = vChans[i];

					CString state = GetNV(pChan->GetName().AsLower());
					if ((GetNV(pChan->GetName().AsLower()) == "1") &&
						(pChan->GetName().AsLower().WildCmp(sCommand.Token(1).AsLower())))
					{
						MCString::iterator it = FindNV(pChan->GetName().AsLower());
						if (it != EndNV())
							DelNV(it);

						PutModule("Disabled automatic reop on deop on: " + pChan->GetName());
					};
				};
			};
        }
		else if ((sCmdName == "status") && (!sChannel.empty())) {
			if (sCommand.Token(1).AsLower() == "all") {
				const vector<CChan*>& vChans = m_pNetwork->GetChans();
				for (unsigned int i = 0; i < vChans.size(); i++ ) {
					const CChan* pChan = vChans[i];

					CString state = GetNV(pChan->GetName().AsLower());
					if (state == "1") {
						PutModule("Automatic reop is currently enabled on " + pChan->GetName() + ".");
					}
					else {
						PutModule("Automatic reop is currently disabled on " + pChan->GetName() + ".");
					}
				};
			}
			else {
				/*
				CString state = GetNV(sChannel);
				if (state == "1") {
					PutModule("Automatic reop is currently enabled on " + sCommand.Token(1) + ".");
				}
				else {
					PutModule("Automatic reop is currently disabled on " + sCommand.Token(1) + ".");
				}
				*/
				const vector<CChan*>& vChans = m_pNetwork->GetChans();
				for (unsigned int i = 0; i < vChans.size(); i++ ) {
					const CChan* pChan = vChans[i];

					CString state = GetNV(pChan->GetName().AsLower());
					if (pChan->GetName().AsLower().WildCmp(sCommand.Token(1).AsLower())) {
						if (state == "1") {
							PutModule("Automatic unban is currently ENABLED on " + pChan->GetName() + ".");
						}
						else {
							PutModule("Automatic unban is currently DISABLED on " + pChan->GetName() + ".");
						}
					};
				};
			}
		}
		else {
			PutModule("Usage: enable|disable|status #channel");
		}

	};

	//Perform the ReOp after a DeOp

	virtual void OnDeop(const CNick& OpNick, const CNick& Nick, CChan& Channel, bool bNoChange) {
		if ( (Nick.GetNick() == m_pNetwork->GetIRCNick().GetNick()) && (OpNick.GetNick() != Nick.GetNick()) ) {
			CString sChannel = Channel.GetName();
			sChannel.MakeLower();

			if (GetNV(sChannel) == "1") {
				PutIRC("cs op " + Channel.GetName() + " " + Nick.GetNick());
				
				PutModule("The nick (" + OpNick.GetNick() + ") deopped you on (" + Channel.GetName() + ")" );
			}

		}

	}
};



template<> void TModInfo<CReOpOnDeOpMod>(CModInfo& Info) {
	Info.SetWikiPage("");
	Info.SetHasArgs(false);
	Info.AddType(CModInfo::NetworkModule);
	Info.SetArgsHelpText("ReOp On DeOp Module.");
}



NETWORKMODULEDEFS(CReOpOnDeOpMod, "ReOp on DeOp module.")



